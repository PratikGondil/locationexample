package com.locationexample.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.locationexample.R;
import com.locationexample.acitivities.ListActivity;
import com.locationexample.base.MasterFragment;


public class ChooseOptionsFragment extends MasterFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Button btnLocation;
    Button btnList;


    public ChooseOptionsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_choose_options, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {

        btnLocation = (Button) view.findViewById(R.id.btnLocation);
        btnList = (Button) view.findViewById(R.id.btnList);

        btnList.setOnClickListener(this);
        btnLocation.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnList:
                Intent intent = new Intent(getActivity(), ListActivity.class);
                startActivity(intent);

                break;

            case R.id.btnLocation:
                replaceCurrentFrament(new LocationUsingServiceFragment());
                break;
        }

    }
}
