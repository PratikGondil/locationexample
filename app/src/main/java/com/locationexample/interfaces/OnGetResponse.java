package com.locationexample.interfaces;

/**
 * Created by pravina on 26/6/18.
 */

public interface OnGetResponse {
    void onGetResponse(String response, boolean isError);
}
