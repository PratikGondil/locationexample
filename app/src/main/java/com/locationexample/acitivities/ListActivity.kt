package com.locationexample.acitivities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerviewmultipleitemlayout.ItemArrayAdapter
import com.example.recyclerviewmultipleitemlayout.ItemObj
import com.example.recyclerviewmultipleitemlayout.ItemType
import com.locationexample.R
import com.locationexample.utils.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.activity_main.*

import java.util.ArrayList

class ListActivity : AppCompatActivity() {


    var count = 1;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        // Initializing list view with the custom adapter
        val itemList = ArrayList<ItemObj>()

        val itemArrayAdapter = ItemArrayAdapter(itemList)
        //item_list.addItemDecoration(SimpleDividerItemDecoration(this))
        item_list.layoutManager = LinearLayoutManager(this)
        item_list.itemAnimator = DefaultItemAnimator()
        item_list.adapter = itemArrayAdapter


        // Populating list items
        for (i in 0..39) {


            when (count) {
                1 -> {
                    itemList.add(ItemObj("ItemObj " + i, ItemType.ONE_ITEM))
                    count = 2
                }
                2 -> {
                    itemList.add(ItemObj("ItemObj " + i, ItemType.TWO_ITEM))
                    count = 3
                }
                3 -> {
                    itemList.add(ItemObj("ItemObj " + i, ItemType.THREE_ITEM))
                    count = 4
                }
                4 -> {
                    itemList.add(ItemObj("ItemObj " + i, ItemType.FOUR_ITEM))
                    count = 1
                }

            }

        }
    }
}
