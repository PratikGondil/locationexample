package com.locationexample.acitivities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;

import com.locationexample.R;
import com.locationexample.base.MasterActivity;
import com.locationexample.fragments.ChooseOptionsFragment;
import com.locationexample.fragments.LocationUsingServiceFragment;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;


public class MainActivity extends MasterActivity {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        replaceFragmentSignUp(new ChooseOptionsFragment());
    }


    /*  @Override
        public void onBackPressed() {

            exitByBackKey();
        }
    */
    protected void exitByBackKey() {

       /* AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to exit application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();*/

    }
}
