package com.example.recyclerviewmultipleitemlayout

enum class ItemType {
    ONE_ITEM, TWO_ITEM,THREE_ITEM,FOUR_ITEM
}
