package com.example.recyclerviewmultipleitemlayout

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.locationexample.R

import java.util.ArrayList

class ItemArrayAdapter(private val itemList: ArrayList<ItemObj> = ArrayList()) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    // get the size of the list
    override fun getItemCount(): Int = itemList.size

    // determine which layout to use for the row
    override fun getItemViewType(position: Int): Int {
        val item = itemList!![position]
        return if (item.type == ItemType.ONE_ITEM) {
            TYPE_ONE
        } else if (item.type == ItemType.TWO_ITEM) {
            TYPE_TWO
        } else if (item.type == ItemType.THREE_ITEM) {
            TYPE_THREE
        } else if (item.type == ItemType.FOUR_ITEM) {
            TYPE_FOUR
        } else {
            -1
        }
    }


    // specify the row layout file and click for each row
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_ONE) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_type1, parent, false)
            return ViewHolderOne(view)
        } else if (viewType == TYPE_TWO) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_type2, parent, false)
            return ViewHolderTwo(view)
        }else if (viewType == TYPE_THREE) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_type3, parent, false)
            return ViewHolderThree(view)
        }else if (viewType == TYPE_FOUR) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_type4, parent, false)
            return ViewHolderFour(view)
        } else {
            throw RuntimeException("The type has to be ONE or TWO") as Throwable
        }
    }

    // load data in each row element
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, listPosition: Int) {
        when (holder.itemViewType) {
            TYPE_ONE -> initLayoutOne(holder as ViewHolderOne, listPosition)
            TYPE_TWO -> initLayoutTwo(holder as ViewHolderTwo, listPosition)
            TYPE_THREE -> initLayoutThree(holder as ViewHolderThree, listPosition)
            TYPE_FOUR -> initLayoutFour(holder as ViewHolderFour, listPosition)
            else -> {
            }
        }
    }

    private fun initLayoutOne(holder: ViewHolderOne, pos: Int) {
        //holder.item.text = itemList!![pos].name
    }

    private fun initLayoutTwo(holder: ViewHolderTwo, pos: Int) {

    }
    private fun initLayoutThree(holder: ViewHolderThree, pos: Int) {

    }

    private fun initLayoutFour(holder: ViewHolderFour, pos: Int) {

    }

    // Static inner class to initialize the views of rows
    internal class ViewHolderOne(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var item: TextView

        init {
            item = itemView.findViewById(R.id.row_item) as TextView
        }
    }

    inner class ViewHolderTwo(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

        }
    }
    inner class ViewHolderThree(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

        }
    }

    inner class ViewHolderFour(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

        }
    }
    companion object {
        private val TYPE_ONE = 1
        private val TYPE_TWO = 2
        private val TYPE_THREE = 3
        private val TYPE_FOUR = 4

    }
}
